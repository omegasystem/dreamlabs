# -*- coding : utf-8 -*-
{
    'name': 'Salesperson permission for Own Customer in Odoo ',
    'version': '15.0.0.0',
    'category': 'Sales',
    'summary': 'Salesperson permission for Own Customer in Odoo',
    'description': """Salesperson permission for Own Customer in Odoo
    """,
    'author': 'Omega System',
    'website': 'https://omegasystem.in/',
    'depends': ['base', 'sale', 'product', 'stock', 'contacts'],
    'data': [
        'views/product_view.xml',
        'views/customer_views.xml',
        'views/stock_view.xml',
        'views/custom_header.xml',
        'views/delivery_report.xml',
        'views/sale_report.xml',
        'views/invoice_report.xml'
        # 'security/sales_person.xml',
        # 'security/ir.model.access.csv',
        # 'wizard/allow_customers_views.xml',
        # 'views/customer_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'license': 'OPL-1',

}
