# -*- coding : utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class ResPartner(models.Model):
    _inherit = "res.partner"

    sap_id = fields.Char('SAP ID')
    customer_currency_id = fields.Many2one('res.currency', string='Customer Currency')
