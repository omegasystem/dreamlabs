# -*- coding : utf-8 -*-
from odoo import models, fields, api, _


class StockPicking(models.Model):
    _inherit = "stock.picking"

    cartons = fields.Integer('Cartons')
    pallet = fields.Integer('Pallet')
