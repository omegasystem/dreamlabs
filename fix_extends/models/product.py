# -*- coding : utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class ProductTemplate(models.Model):
    _inherit = "product.template"

    design_number = fields.Char('Design NUMBER')
    group = fields.Char('Group')
    color = fields.Char('Color')
    tube_length = fields.Char('TUBE LENGTH')
    kosmos_standard = fields.Char('KOSMOS STANDARD')
    miscellaneous_kosmos = fields.Char('Miscellaneous Kosmos')
    merlin = fields.Char('Merlin')
    Schaumkopfe = fields.Char('SCHAUMKOPFE 02-31xx')
    caps = fields.Char('CAPS')
    Schaumkopf_city = fields.Char('SCHAUMKOPF CITY & IRIS')
    Kosmos_mini = fields.Char('KOSMOS MINI - POWERJET')
    manual_assembly = fields.Char('MANUAL ASSEMBLY')
    automated_assembly = fields.Char('AUTOMATED ASSEMBLY')
    row_material = fields.Char('RAW MATERIAL')
    dyes = fields.Char('DYES - MASTERBATCH')
    gross_weight = fields.Char('GROSS WEIGHT')
    pieces_per_packing = fields.Char('PIECES FOR PACKING')
    sap_id = fields.Char('SAP ID')
    supplier_code = fields.Char('Supplier Code')
